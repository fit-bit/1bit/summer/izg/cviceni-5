IZG Cvičení 5
-------------
Body: 3/3

#### Kompilace
Vše kde je rozbalit archiv se cvičením se u tohoto neprovadí, obsah repozitáže už je rozbalený archiv se cvičením.
##### Prerekvizity
Aplikace je závislá na knihovně Simple Directmedia Layer. Je nutné mít nainstalovanou development verzi knihovny. Na školních počítačích je tato knihovna instalována (viz jednotlivé návody).

##### Spuštění a kompilace pomocí MinGW v prostředí Windows na školních počítačích
Stáhnout si soubor se cvičením a rozbalit
V adresáři Q:\mingw spustit Run Mingw.lnk - otevře se MinGW konzole
V MinGW konzoli se přepnout do adresáře se cvičením - cd ...
Pomocí příkazu mingw32-make projekt přeložit
Spustit pomocí vytvořeného .exe souboru v konzoli MinGW (nutné kvůli nalezení .dll knihoven)

##### Spuštění a kompilace v prostředí MS Visual Studio 2010
Stáhnout si soubor se cvičením a rozbalit
Stáhnout si Windows development verzi knihovny SDL (http://www.libsdl.org/release/SDL-devel-1.2.15-VC.zip)
Tuto knihovnu rozbalit do (SLOŽKA SE CVIČENÍM)\sdl-1.2.15 (již připravena)
Otevřít soubor .sln v prostředí MS Visual Studio
Příkazy prostředí MS Visual Studio:
- F7 = Kompilace
- F5 = Kompilace + Spuštění v debug módu
- Ctrl + F5 = Kompilace + Spuštění

##### Spuštění a kompilace v Linuxovém prostředí
Stáhnout si soubor se cvičením a rozbalit
```
unzip izg_lab_XX.zip
```
Kompilace pomocí make
```
make
```
Ak kompilace selže na chybějícím SDL (na Ubuntu/Debian/...):
```
sudo apt-get install libsdl1.2-dev
make
```
Standardně spustit cvičení
```
./izg_lab_XX
```

#### Ovládání aplikace
##### Key a:
Spuštění animace (ak je animace příliš rychlá, zvyšte hodnotu konstanty delay v soubore main.cpp napr. na hodnotu 5)